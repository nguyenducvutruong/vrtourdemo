﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hotspot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    [SerializeField] GameObject m_TargetSphere;
    [SerializeField] Vector3 m_NewScale = new Vector3(0.02f, 0.02f, 0.02f);
    private GameObject m_ThisSphere;
    private Vector3 m_OriginalScale;
    private Renderer m_Renderer;

    void Awake()
    {
        m_Renderer = this.GetComponent<Renderer>();
    }

    void Start()
    {
        m_ThisSphere = this.transform.parent.gameObject;
        m_OriginalScale = this.transform.localScale;
    }

    private void Update()
    {
        if(TourManager.instance.GetCameraPos().position == this.transform.position)
        {
            m_Renderer.enabled = true;
            TourManager.instance.Stop();

            m_TargetSphere.SetActive(true);
            m_ThisSphere.SetActive(false);

            TourManager.SetCameraPosition(m_TargetSphere.transform.position, this.transform.position);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnPointerExit(eventData);
        if (TourManager.SetCameraPosition != null && TourManager.MoveCameraTowardInfo != null)
        {
            m_Renderer.enabled = false;
            Debug.Log("Move");
            TourManager.MoveCameraTowardInfo(this.transform);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("Pointer Enter");
        this.transform.localScale = m_NewScale;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Pointer Exit");
        this.transform.localScale = m_OriginalScale;
    }
}
