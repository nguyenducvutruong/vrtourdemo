﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TourManager : MonoBehaviour
{
    public static TourManager instance = null;


    [SerializeField] GameObject[] m_AllSpheres;
    [SerializeField] Transform m_MainCamera;

    public delegate void SetCameraPositionHandler(Vector3 position, Vector3 direction);
    public static SetCameraPositionHandler SetCameraPosition;
    public delegate void MoveCameraTowardTarget(Transform target);
    public static MoveCameraTowardTarget MoveCameraTowardInfo;

    
    private void Awake()
    {
        // Setup singleton
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        DisableOtherSpheresExcept(m_AllSpheres[0]);
    }

    private void OnEnable()
    {
        SetCameraPosition += SetCamera;
        MoveCameraTowardInfo += CameraTransition;
    }

    private void OnDisable()
    {
        SetCameraPosition -= SetCamera;
        MoveCameraTowardInfo -= CameraTransition;
    }
    private void DisableOtherSpheresExcept(GameObject remainingSphere)
    {
        foreach(GameObject sphere in m_AllSpheres)
        {
            sphere.SetActive(false);
        }
        remainingSphere.SetActive(true);
    }
    public void CameraTransition(Transform target)
    {
        StartCoroutine(MoveTowardTarget(target.position));
    }
    public void SetCamera(Vector3 pos, Vector3 dir)
    {
        m_MainCamera.transform.parent.position = pos;
        m_MainCamera.transform.LookAt(dir);
    }
    IEnumerator MoveTowardTarget(Vector3 target)
    {
        while(this.transform.parent.position != target)
        {
            this.transform.parent.position = Vector3.MoveTowards(this.transform.parent.position, target, 2 * Time.deltaTime);
            yield return null;
        }
    }
    public void Stop()
    {
        StopAllCoroutines();
    }
    public Transform GetCameraPos()
    {
        return m_MainCamera.transform.parent;
    }
}
